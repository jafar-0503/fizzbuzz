package com.hackerrank.restcontrolleradvice.exception;

import java.util.HashMap;
import java.util.Map;

import com.hackerrank.restcontrolleradvice.dto.BuzzException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.hackerrank.restcontrolleradvice.dto.FizzBuzzException;
import com.hackerrank.restcontrolleradvice.dto.FizzException;

@RestControllerAdvice
public class FizzBuzzExceptionHandler extends ResponseEntityExceptionHandler {

  //TODO:: implement handler methods for FizzException, BuzzException and FizzBuzzException
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(FizzException.class)
    public Map<String, Object> fizzException(FizzException ex){
        Map<String, Object> result = new HashMap<>();
        result.put("message", ex.getMessage());
        result.put("errorReason", ex.getErrorReason());

        return result;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BuzzException.class)
    public Map<String, Object> buzzException(BuzzException ex){
        Map<String, Object> result = new HashMap<>();
        result.put("message", ex.getMessage());
        result.put("errorReason", ex.getErrorReason());

        return result;
    }

    @ResponseStatus(HttpStatus.INSUFFICIENT_STORAGE)
    @ExceptionHandler(FizzBuzzException.class)
    public Map<String, Object> fizzBuzzException(FizzBuzzException ex){
        Map<String, Object> result = new HashMap<>();
        result.put("message", ex.getMessage());
        result.put("errorReason", ex.getErrorReason());

        return result;
    }
}
